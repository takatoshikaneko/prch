<?php
App::uses('Content', 'Model');

/**
 * Content Test Case
 */
class ContentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.content',
		'app.catalog',
		'app.catalog_series',
		'app.shop',
		'app.coordination',
		'app.contents',
		'app.goods_information',
		'app.coordination_picture'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Content = ClassRegistry::init('Content');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Content);

		parent::tearDown();
	}

}
