<?php
App::uses('CoordinationPicture', 'Model');

/**
 * CoordinationPicture Test Case
 */
class CoordinationPictureTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.coordination_picture',
		'app.coordination',
		'app.shop',
		'app.catalog_series',
		'app.catalog',
		'app.content',
		'app.contents',
		'app.goods_information'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CoordinationPicture = ClassRegistry::init('CoordinationPicture');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CoordinationPicture);

		parent::tearDown();
	}

}
