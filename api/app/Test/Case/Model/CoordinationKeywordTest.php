<?php
App::uses('CoordinationKeyword', 'Model');

/**
 * CoordinationKeyword Test Case
 */
class CoordinationKeywordTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.coordination_keyword',
		'app.coordination',
		'app.shop',
		'app.catalog_series',
		'app.catalog',
		'app.content',
		'app.contents',
		'app.goods_information',
		'app.coordination_picture'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CoordinationKeyword = ClassRegistry::init('CoordinationKeyword');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CoordinationKeyword);

		parent::tearDown();
	}

}
