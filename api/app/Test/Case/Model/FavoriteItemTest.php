<?php
App::uses('FavoriteItem', 'Model');

/**
 * FavoriteItem Test Case
 */
class FavoriteItemTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.favorite_item',
		'app.favorite_list',
		'app.user',
		'app.coordination',
		'app.shop',
		'app.catalog_series',
		'app.catalog',
		'app.content',
		'app.contents',
		'app.coordination_category',
		'app.category',
		'app.channel',
		'app.coordination_channel',
		'app.cannel',
		'app.coordination_keyword',
		'app.coordination_picture',
		'app.goods_information'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->FavoriteItem = ClassRegistry::init('FavoriteItem');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->FavoriteItem);

		parent::tearDown();
	}

}
