<?php
App::uses('CoordinationCategoly', 'Model');

/**
 * CoordinationCategoly Test Case
 */
class CoordinationCategolyTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.coordination_categoly',
		'app.coordination',
		'app.shop',
		'app.catalog_series',
		'app.catalog',
		'app.content',
		'app.contents',
		'app.goods_information',
		'app.coordination_picture',
		'app.categoly'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CoordinationCategoly = ClassRegistry::init('CoordinationCategoly');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CoordinationCategoly);

		parent::tearDown();
	}

}
