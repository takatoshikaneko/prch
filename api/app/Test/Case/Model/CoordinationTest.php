<?php
App::uses('Coordination', 'Model');

/**
 * Coordination Test Case
 */
class CoordinationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.coordination',
		'app.shop',
		'app.catalog_series',
		'app.catalog',
		'app.content',
		'app.contents',
		'app.goods_information',
		'app.coordination_picture'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Coordination = ClassRegistry::init('Coordination');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Coordination);

		parent::tearDown();
	}

}
