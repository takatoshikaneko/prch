<?php
App::uses('GoodsInformation', 'Model');

/**
 * GoodsInformation Test Case
 */
class GoodsInformationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.goods_information',
		'app.coordination',
		'app.shop',
		'app.catalog_series',
		'app.catalog',
		'app.series',
		'app.content',
		'app.coordination_categoly',
		'app.contents',
		'app.coordination_picture'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->GoodsInformation = ClassRegistry::init('GoodsInformation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->GoodsInformation);

		parent::tearDown();
	}

}
