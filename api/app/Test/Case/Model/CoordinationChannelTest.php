<?php
App::uses('CoordinationChannel', 'Model');

/**
 * CoordinationChannel Test Case
 */
class CoordinationChannelTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.coordination_channel',
		'app.coordination',
		'app.shop',
		'app.catalog_series',
		'app.catalog',
		'app.content',
		'app.contents',
		'app.coordination_categoly',
		'app.coordination_keyword',
		'app.coordination_picture',
		'app.goods_information',
		'app.cannel'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CoordinationChannel = ClassRegistry::init('CoordinationChannel');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CoordinationChannel);

		parent::tearDown();
	}

}
