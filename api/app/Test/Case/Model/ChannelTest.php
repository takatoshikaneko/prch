<?php
App::uses('Channel', 'Model');

/**
 * Channel Test Case
 */
class ChannelTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.channel',
		'app.category',
		'app.coordination',
		'app.shop',
		'app.catalog_series',
		'app.catalog',
		'app.content',
		'app.contents',
		'app.coordination_categoly',
		'app.coordination_keyword',
		'app.coordination_picture',
		'app.goods_information',
		'app.coordination_channel'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Channel = ClassRegistry::init('Channel');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Channel);

		parent::tearDown();
	}

}
