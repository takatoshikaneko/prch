<?php
App::uses('CatalogSeries', 'Model');

/**
 * CatalogSeries Test Case
 */
class CatalogSeriesTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.catalog_series',
		'app.shop',
		'app.coordination',
		'app.catalog',
		'app.series',
		'app.content',
		'app.coordination_categoly',
		'app.contents',
		'app.goods_information',
		'app.coordination_picture'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CatalogSeries = ClassRegistry::init('CatalogSeries');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CatalogSeries);

		parent::tearDown();
	}

}
