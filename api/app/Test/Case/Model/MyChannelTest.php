<?php
App::uses('MyChannel', 'Model');

/**
 * MyChannel Test Case
 */
class MyChannelTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.my_channel',
		'app.user',
		'app.cannel'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MyChannel = ClassRegistry::init('MyChannel');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MyChannel);

		parent::tearDown();
	}

}
