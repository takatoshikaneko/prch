<?php
/**
 * CoordinationPicture Fixture
 */
class CoordinationPictureFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'coordination_picture';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'coordination_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'coordination_picture_url' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'coordination_picture_type' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'coordination_picture_width' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'coordination_picture_height' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'display_sequence' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'substitution_picture_url' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'valid_flag' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'coordination_id' => 1,
			'coordination_picture_url' => 'Lorem ipsum dolor sit amet',
			'coordination_picture_type' => 1,
			'coordination_picture_width' => 1,
			'coordination_picture_height' => 1,
			'display_sequence' => 1,
			'substitution_picture_url' => 'Lorem ipsum dolor sit amet',
			'created' => '2016-11-15 19:12:06',
			'modified' => '2016-11-15 19:12:06',
			'valid_flag' => 1
		),
	);

}
