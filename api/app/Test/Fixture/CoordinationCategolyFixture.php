<?php
/**
 * CoordinationCategoly Fixture
 */
class CoordinationCategolyFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'coordination_categoly';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'coordination_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'categoly_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'valid_flag' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'coordination_id' => 1,
			'categoly_id' => 1,
			'created' => '2016-11-15 19:11:20',
			'modified' => '2016-11-15 19:11:20',
			'valid_flag' => 1
		),
	);

}
