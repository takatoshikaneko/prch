<?php
/**
 * FavoriteItem Fixture
 */
class FavoriteItemFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'favorite_item';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'favorite_list_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'coordination_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'display_order' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 8, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'valid_flag' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'favorite_list_id' => 1,
			'coordination_id' => 1,
			'display_order' => 1,
			'created' => '2016-11-24 18:04:39',
			'modified' => '2016-11-24 18:04:39',
			'valid_flag' => 1
		),
	);

}
