<?php
/**
 * GoodsInformation Fixture
 */
class GoodsInformationFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'goods_information';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'coordination_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'coordination_picture_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'link_position_x' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'link_position_y' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'goods_url' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'goods_url_af' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'goods_name' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'goods_price_text' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'goods_price' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'display_sequence' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'valid_flag' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'coordination_id' => 1,
			'coordination_picture_id' => 1,
			'link_position_x' => 1,
			'link_position_y' => 1,
			'goods_url' => 'Lorem ipsum dolor sit amet',
			'goods_url_af' => 'Lorem ipsum dolor sit amet',
			'goods_name' => 'Lorem ipsum dolor sit amet',
			'goods_price_text' => 'Lorem ipsum dolor sit amet',
			'goods_price' => 1,
			'display_sequence' => 1,
			'created' => '2016-11-15 19:01:58',
			'modified' => '2016-11-15 19:01:58',
			'valid_flag' => 1
		),
	);

}
