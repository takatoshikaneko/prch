<?php
/**
 * Coordination Fixture
 */
class CoordinationFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'coordination';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'coordination_publish_starting_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'coordination_publish_end_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'shop_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'catalog_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'contents_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'carrying_page_url' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'carrying_page_url_af' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'carrying_page' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'carrying_page_text' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'coordination_page_url' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'coordination_page_url_af' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'valid_flag' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'coordination_publish_starting_date' => '2016-11-15 19:10:57',
			'coordination_publish_end_date' => '2016-11-15 19:10:57',
			'shop_id' => 1,
			'catalog_id' => 1,
			'contents_id' => 1,
			'carrying_page_url' => 'Lorem ipsum dolor sit amet',
			'carrying_page_url_af' => 'Lorem ipsum dolor sit amet',
			'carrying_page' => 'Lorem ipsum dolor sit amet',
			'carrying_page_text' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'coordination_page_url' => 'Lorem ipsum dolor sit amet',
			'coordination_page_url_af' => 'Lorem ipsum dolor sit amet',
			'created' => '2016-11-15 19:10:57',
			'modified' => '2016-11-15 19:10:57',
			'valid_flag' => 1
		),
	);

}
