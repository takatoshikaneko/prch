<?php
define('INTERNAL_ERROR_CODE', 'EC001');

$config['USER_ATH_MESSAGE'][] = array('MU0001', 'ログインに成功しました');
$config['USER_ATH_MESSAGE'][] = array('MU0002', 'ログインが必要です');
$config['USER_ATH_MESSAGE'][] = array('MU0003', 'ログインに失敗しました');
$config['USER_ADD_MESSAGE'] = array('MU0101', 'ユーザの登録に成功しました');
$config['USER_EDT_MESSAGE'] = array('MU0201', 'ユーザの更新に成功しました');
$config['USER_DEL_MESSAGE'] = array('MU0301', 'ユーザの削除に成功しました');
$config['USER_VIW_MESSAGE'] = array('MU0401', '該当するデータがありません');
$config['USER_IDX_MESSAGE'] = array('MU0501', '登録されているデータがありません');
$config['USER_PRV_MESSAGE'][] = array('MU0601', '仮登録ユーザの照会に成功しました');
$config['USER_PRV_MESSAGE'][] = array('MU0602', '仮登録ユーザの照会が出来ませんでした。無効なURLです。');


