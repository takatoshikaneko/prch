<?php
App::uses('AppModel', 'Model');
/**
 * FavoriteItem Model
 *
 * @property FavoriteList $FavoriteList
 * @property Coordination $Coordination
 */
class FavoriteItem extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'favorite_item';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'favorite_list_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'coordination_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'display_order' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'valid_flag' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'FavoriteList' => array(
			'className' => 'FavoriteList',
			'foreignKey' => 'favorite_list_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Coordination' => array(
			'className' => 'Coordination',
			'foreignKey' => 'coordination_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
