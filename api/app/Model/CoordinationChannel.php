<?php
App::uses('AppModel', 'Model');
/**
 * CoordinationChannel Model
 *
 * @property Coordination $Coordination
 * @property Cannel $Cannel
 */
class CoordinationChannel extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'coordination_channel';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'coordination_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cannel_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'valid_flag' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Coordination' => array(
			'className' => 'Coordination',
			'foreignKey' => 'coordination_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cannel' => array(
			'className' => 'Cannel',
			'foreignKey' => 'cannel_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
