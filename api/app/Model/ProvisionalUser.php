<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

/**
 * User Model
 *
 */
class ProvisionalUser extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

/**
 * beforeSave method
 *
 * @param  array $options
 * @return boolean
 */
    public function beforeSave($options = array()) {

        parent::beforeSave($options);

        if (isset($this->data[$this->alias]['email'])) {
            $passwordHasher = new SimplePasswordHasher();
            $this->data[$this->alias]['token'] = $passwordHasher->hash($this->data[$this->alias]['email']);
        }

        return true;
    }
}
