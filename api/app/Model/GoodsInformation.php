<?php
App::uses('AppModel', 'Model');
/**
 * GoodsInformation Model
 *
 * @property Coordination $Coordination
 * @property CoordinationPicture $CoordinationPicture
 */
class GoodsInformation extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'goods_information';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'coordination_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'display_sequence' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'valid_flag' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Coordination' => array(
			'className' => 'Coordination',
			'foreignKey' => 'coordination_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CoordinationPicture' => array(
			'className' => 'CoordinationPicture',
			'foreignKey' => 'coordination_picture_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
