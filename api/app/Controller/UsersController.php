<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator' , 'Flash');

/**
 * beforeFilter method
 *
 * @return void
 */
	public function beforeFilter() {
	    parent::beforeFilter();
	    $this->Auth->allow();
	    $this->Auth->deny(
	    					'view'
	    					);
// 	    $this->Auth->allow(
// 	    					'login' ,
// 	    					'authorized',
// 	    					'unauthorized',
// 	    					'index',
// 	    					'view',
// 	    					'edit',
// 	    					'delete',
// 	    					'opauthComplete',
// 	    					'provisionalRegist',
// 	    					'registByEmail',
// 	    					'add'
// 	    					);
		$this->autoRender = false;
		$this->response->type('json');
	}

/**
 * authorized method
 *
 * @return json
 */
	public function authorized() {
		$msgConst = Configure::read('USER_ATH_MESSAGE');
		return $this->Common->returnResult($this->request->is('ajax'), null, $msgConst[0][1], $msgConst[0][0]);
	}

/**
 * unauthorized method
 *
 * @return json
 */
	public function unauthorized() {
		$msgConst = Configure::read('USER_ATH_MESSAGE');
		return $this->Common->returnResult($this->request->is('ajax'), null, $msgConst[1][1], $msgConst[1][0]);
	}

/**
 * login method
 *
 * @return json
 */
	public function login() {
// 		if ($this->request->is('post')) {
		if (true) {
			if ($this->Auth->login()) {
				$this->redirect($this->Auth->loginRedirect);
			} else {
				$msgConst = Configure::read('USER_ATH_MESSAGE');
				return $this->Common->returnResult($this->request->is('ajax'), null, $msgConst[2][1], $msgConst[2][0]);
			}
		}
	}

/**
 * logout method
 *
 * @return void
 */
	public function logout() {
		$this->redirect($this->Auth->logout());
	}

/**
 * index method
 *
 * @return json
 */
	public function index() {
		try {

			$msgConst = Configure::read('USER_IDX_MESSAGE');
			$this->User->recursive = 0;
			$result = $this->Paginator->paginate();

			return $this->Common->returnResult($this->request->is('ajax'), $result, $msgConst[1], $msgConst[0]);

		} catch(Exception $e) {
			return $this->Common->returnResult($this->request->is('ajax'), null, $e->getMessage(), INTERNAL_ERROR_CODE);
		}
	}

/**
 * view method
 *
 * @param string $id
 * @return json
 */
	public function view($id = null) {
		try {
			// TODO POSTにできる？

			$msgConst = Configure::read('USER_VIW_MESSAGE');
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$result = $this->User->find('first', $options);

			return $this->Common->returnResult($this->request->is('ajax'), $result, $msgConst[1], $msgConst[0]);

		} catch(Exception $e) {
			return $this->Common->returnResult($this->request->is('ajax'), null, $e->getMessage(), INTERNAL_ERROR_CODE);
		}
	}

/**
 * provisionalRegist method
 *
 * @return void
 */
	public function provisionalRegist() {
		try {
			$this->loadModel('ProvisionalUser');

			$msgConst = Configure::read('USER_ADD_MESSAGE');
			if ($this->request->is('post')) {
				// TODO Validation
				$this->ProvisionalUser->create();
				$params = array(
								'validate' => true,
								'fieldlist' => array('email', 'token'),
								);

				if ($result = $this->ProvisionalUser->save($this->request->data, $params)) {
					$retMsg = $msgConst[1];
				} else {
					// Throw Exception.
					$retMsg = $this->ProvisionalUser->validationErrors;
				}
				// TODO Mail Message
				$Email = new CakeEmail();
				$Email->to($this->request->data['email'])
					  ->send('My message');
			} else {
				// This request doesn't send by post.
			}

			return $this->Common->returnResult($this->request->is('ajax'), $result, $retMsg, $msgConst[0]);

		} catch(Exception $e) {
			return $this->Common->returnResult($this->request->is('ajax'), null, $e->getMessage(), INTERNAL_ERROR_CODE);
		}
	}

/**
 * registByEmail method
 *
 * @return json
 */
	public function registByEmail($email = null, $token = null) {
		try {
			$this->loadModel('ProvisionalUser');

			$msgConstArr = Configure::read('USER_PRV_MESSAGE');
			// TODO Validation
// 			$options = array('conditions' => array('ProvisionalUser.email' => $this->request->data['email']));
// 			$result = $this->ProvisionalUser->find('first', $options);

			if ($result = $this->ProvisionalUser->findByEmailAndToken($email, $token)) {
				$msgConst = $msgConstArr[0];
				// TODO レコード削除
			} else {
				// Throw Exception.
				$msgConst = $msgConstArr[1];
			}

			return $this->Common->returnResult($this->request->is('ajax'), $result, $msgConst[1], $msgConst[0]);

		} catch(Exception $e) {
			return $this->Common->returnResult($this->request->is('ajax'), null, $e->getMessage(), INTERNAL_ERROR_CODE);
		}
	}

/**
 * passwordReissue method
 *
 * @return void
 */
	public function passwordReissue() {
		try {
			$msgConst = Configure::read('USER_ADD_MESSAGE');
			if ($this->request->is('post')) {
				// ユーザ検索(Email)
				// *OKならパスワード再発行

				// TODO Mail Message
				$Email = new CakeEmail();
				$Email->to($this->request->data['email'])
					  ->send('My message');
			} else {
				// This request doesn't send by post.
			}

			return $this->Common->returnResult($this->request->is('ajax'), $result, $retMsg, $msgConst[0]);

		} catch(Exception $e) {
			return $this->Common->returnResult($this->request->is('ajax'), null, $e->getMessage(), INTERNAL_ERROR_CODE);
		}
	}

/**
 * add method
 *
 * @return json
 */
	public function add() {
		try {

			$msgConst = Configure::read('USER_ADD_MESSAGE');
			if ($this->request->is('post')) {
				// TODO Validation
				$this->User->create();
				if ($result = $this->User->save($this->request->data)) {
					$retMsg = $msgConst[1];
				} else {
					// Throw Exception.
					$retMsg = $this->User->validationErrors;
				}
			} else {
				// This request doesn't send by post.
			}

	// 		$result = $this->Paginator->paginate();
			return $this->Common->returnResult($this->request->is('ajax'), $result, $retMsg, $msgConst[0]);

		} catch(Exception $e) {
			return $this->Common->returnResult($this->request->is('ajax'), null, $e->getMessage(), INTERNAL_ERROR_CODE);
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return json
 */
	public function edit() {
		try {

			$msgConst = Configure::read('USER_EDT_MESSAGE');
			if ($this->request->is('post')) {
				// TODO Validation
				// TODO User Check
				if ($result = $this->User->save($this->request->data)) {
					$retMsg = $msgConst[1];
				} else {
					// Throw Exception.
					$retMsg = $this->User->validationErrors;
				}
			} else {
				// This request doesn't send by post.
			}

			return $this->Common->returnResult($this->request->is('ajax'), $result, $retMsg, $msgConst[0]);

		} catch(Exception $e) {
			return $this->Common->returnResult($this->request->is('ajax'), null, $e->getMessage(), INTERNAL_ERROR_CODE);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return json
 */
	public function delete($id = null) {
		try {

			$msgConst = Configure::read('USER_DEL_MESSAGE');
			// Here change the delete_flag
			$this->request->data['User']['id'] = 4;
			$this->request->data['User']['username'] = '+++++';
			if ($this->request->is('post')) {
				// TODO Validation
				// TODO User Check
				if ($result = $this->User->save($this->request->data)) {
					$retMsg = $msgConst[1];
				} else {
					// Throw Exception.
					$retMsg = $this->User->validationErrors;
				}
			} else {
				// This request doesn't send by post.
			}

			return $this->Common->returnResult($this->request->is('ajax'), $result, $retMsg, $msgConst[0]);

		} catch(Exception $e) {
			return $this->Common->returnResult($this->request->is('ajax'), null, $e->getMessage(), INTERNAL_ERROR_CODE);
		}
	}

/**
 * opauthComplete method
 *
 * @throws UnauthorizedException
 * @return json
 */
	public function opauthComplete() {
		if(!is_null($this->Auth->user())){
			$this->redirect($this->Auth->loginRedirect);
		}

		if(empty($this->request->data) || $this->request->data['validated'] === false){
			throw new UnauthorizedException('認証に失敗しました。');
		} else {
			$options = array('contain' => array(),
							 'conditions' => array('User.provider' => $this->request->data['auth']['provider'],
													'User.uid'      => $this->request->data['auth']['uid']));
			// ユーザー情報取得
			$user = $this->User->find('first', $options);

			// 未登録アカウントの場合登録
			if(empty($user)){
				$this->_registUser($options);
			}

			// ログイン処理
			if(!empty($user) && $this->Auth->login($user['User'])){
				$this->redirect($this->Auth->loginRedirect);
			} else {
				throw new UnauthorizedException('ログインに失敗しました。');
			}
		}

// 		$retArr = json_encode($this->data);
// 		return ($this->request->is('ajax'))? $retArr : $this->Common->indent($retArr);
	}

/**
 * regist method
 *
 * @throws UnauthorizedException
 * @return json
 */
	private function _registUser($options){
		// TODO Validation
		$reqUser = array( 'User.provider' => $this->request->data['provider'],
						'User.uid'      => $this->request->data['uid'],
						'User.username' => $this->request->data['auth']['raw']['name']);
		if($this->User->save($reqUser)){
			$user = $this->User->find('first', $options);
		} else {
			throw new InternalErrorException('新規登録に失敗しました。');
		}
	}

/**
 * physical_delete method
 *
 * @param string $id
 * @return json
 */
	public function physical_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Flash->success(__('The user has been deleted.'));
		} else {
			$this->Flash->error(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
