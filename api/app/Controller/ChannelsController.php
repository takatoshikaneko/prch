<?php
App::uses('AppController', 'Controller');
/**
 * Shops Controller
 *
 * @property PaginatorComponent $Paginator
 */
class ChannelsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator' , 'Flash');

/**
 * beforeFilter method
 *
 * @return void
 */
	public function beforeFilter() {
	    parent::beforeFilter();
	    $this->Auth->allow();
	    $this->Auth->deny(
	    					'view'
	    					);
	    $this->autoRender = false;
		$this->response->type('json');
	}

/**
 * getRecommendedChannel method
 *
 * @return json
 */
	public function getRecommendedChannel() {
		return $this->getChannelList(true);
	}

/**
 * getChannelList method
 *
 * @return json
 */
	public function getChannelList($recommend = false) {
		try {

			$Model = 'Channel';
			$this->loadModel($Model);
			// TODO Message
			$msgConst = Configure::read('USER_IDX_MESSAGE');
			$this->$Model->Behaviors->load('Containable');

			// TODO 非ログイン時条件
			if(false){
				$params = array('contain' => false);
				$result = $this->$Model->find('all', $params);
				if($recommend){ $result = array_slice($result, 0, 8); }

			// ログイン時、フィルタ設定に応じてソート
			} else {
				$params['order'] = array('Channel.id DESC');
				$params['contain']['MyChannel']['conditions'] = array('user_id' => 1);
				$params['contain']['CoordinationChannel']['fields'] = array('DISTINCT coordination_id');
				$params['contain']['CoordinationChannel']['Coordination']['CoordinationCategory']['fields'] = array('DISTINCT category_id');

// 				$result = $this->$Model->find('all', $params);
				$result = $this->_getChannelsOrderByUserFilter($this->$Model->find('all', $params));

				if($recommend){ $result = array_slice($result, 0, 8); }
			}

			return $this->Common->returnResult($this->request->is('ajax'), $result, $msgConst[1], $msgConst[0]);

		} catch(Exception $e) {
			return $this->Common->returnResult($this->request->is('ajax'), null, $e->getMessage(), INTERNAL_ERROR_CODE);
		}
	}

/**
 * getChannelsOrderByUserFilter method
 *
 * @return array
 */
	private function _getChannelsOrderByUserFilter($result) {
		// 期待のソート順になるように'display_order'を設定する
		// ログインユーザのお気に入りショップ・カタログ・カテゴリ
		$this->loadModel('User');
		// TODO ユーザID指定
		$user = $this->User->find('first', array('conditions' => array('User.id' => 1)));
		foreach ($result as $k=>$v){
			$result[$k]['display_order'] = 0;
			foreach ($v['CoordinationChannel'] as $key=>$val){
				if(in_array($val['Coordination']['shop_id'],explode(',',$user['User']['favorite_shop']))){
					$result[$k]['display_order'] +=100;
				}
				if(in_array($val['Coordination']['catalog_id'],explode(',',$user['User']['favorite_catalog']))){
					$result[$k]['display_order'] +=100;
				}
				if( count($val['Coordination']['CoordinationCategory']) > 0 &&
					in_array($val['Coordination']['CoordinationCategory'][0]['category_id'],explode(',',$user['User']['favorite_category']))
				){
					$result[$k]['display_order'] +=100;
				}
			}
			if(count($v['MyChannel']) > 0){ $result[$k]['display_order'] +=100; }
		}

		usort($result, function($a, $b) {
			if ($a['display_order'] == $b['display_order']) {
				return 0;
			}
			return ($a['display_order'] > $b['display_order']) ? -1 : 1;
		});

		return $result;
	}

/**
 * changeMyChannelStatus method
 *
 * @return json
 */
	public function changeMyChannelStatus() {
		try {
			$Model = 'MyChannel';
			$this->loadModel($Model);

			// TODO Message
			$msgConst = Configure::read('USER_ADD_MESSAGE');

//			if ($this->request->is('post')) {
			if (true) {
				// TODO Validation
				$params['contain'] = false;
				$params['conditions']['user_id'] = $this->request->data['user_id'];
				$params['conditions']['channel_id'] = $this->request->data['channel_id'];

				if(!$user = $this->$Model->find('first', $params)){
					// ログインユーザ・チャンネルの組み合わせがない
					$this->$Model->create();
				} else {
					// ログインユーザ・チャンネルの組み合わせがある
					$this->request->data['id'] = $user['MyChannel']['id'];
					$this->request->data['valid_flag'] = ($user['MyChannel']['valid_flag'] == 0)? 1:0;
				}

				if ($result = $this->$Model->save($this->request->data)) {
					$retMsg = $msgConst[1];
				} else {
					// Throw Exception.
					$retMsg = $this->$Model->validationErrors;
				}

			} else {
				// This request doesn't send by post.
			}

			return $this->Common->returnResult($this->request->is('ajax'), $result, $retMsg, $msgConst[0]);

		} catch(Exception $e) {
			return $this->Common->returnResult($this->request->is('ajax'), null, $e->getMessage(), INTERNAL_ERROR_CODE);
		}
	}

/**
 * getCoordinationList method
 *
 * @return json
 */
	public function getCoordinationList() {
		try {

			$Model = 'Channel';
			$this->loadModel($Model);
			// TODO Message
			$msgConst = Configure::read('USER_IDX_MESSAGE');
			$this->$Model->Behaviors->load('Containable');

			$params['order'] = array('Channel.id DESC');
			$params['contain']['MyChannel']['conditions'] = array('user_id' => 1);
			$params['contain']['CoordinationChannel']['fields'] = array('DISTINCT coordination_id');
			$params['contain']['CoordinationChannel']['Coordination']['CoordinationCategory']['fields'] = array('DISTINCT category_id');

			// TODO 非ログイン時条件
			if(false){

				$result = $this->$Model->find('all', $params);

			// ログイン時、フィルタ設定に応じてソート
			} else {

				$result = $this->_getChannelsOrderByUserFilter($this->$Model->find('all', $params));
				$result = $this->_getCoordinationsOrderByUserFilter($result);

				// ログイン時、フィルタ設定に応じてソート
				// 期待のソート順になるように'display_order'を仕込んでおく
				// TODO お気に入りとの紐づけ
// 				foreach ($result as $k=>$v){
// 					foreach ($v['CoordinationChannel'] as $key=>$val){
// 						$result[$k]['CoordinationChannel'][$key]['display_order'] = 0;
// 						// TODO 条件。ショップ・カタログ・カテゴリ
// 						if($val['Coordination']['shop_id'] == 3){
// 							$result[$k]['CoordinationChannel'][$key]['display_order'] += 10000;
// 						}
// 					}
// 				}
// 				foreach ($result as $k=>$v){
// 					usort($result[$k]['CoordinationChannel'], function($a, $b) {
// 						if ($a['display_order'] == $b['display_order']) {
// 							return 0;
// 						}
// 						return ($a['display_order'] > $b['display_order']) ? -1 : 1;
// 					});
// 				}
			}

			return $this->Common->returnResult($this->request->is('ajax'), $result, $msgConst[1], $msgConst[0]);

		} catch(Exception $e) {
			return $this->Common->returnResult($this->request->is('ajax'), null, $e->getMessage(), INTERNAL_ERROR_CODE);
		}
	}

/**
 * getCoordinationsOrderByUserFilter method
 *
 * @return array
 */
	private function _getCoordinationsOrderByUserFilter($result) {

// 		foreach ($result as $k=>$v){
// 			foreach ($v['CoordinationChannel'] as $key=>$val){
// 				$result[$k]['CoordinationChannel'][$key]['display_order'] = 0;
// 				// TODO 条件。ショップ・カタログ・カテゴリ
// 				if($val['Coordination']['shop_id'] == 3){
// 					$result[$k]['CoordinationChannel'][$key]['display_order'] += 10000;
// 				}
// 			}
// 		}

		// 期待のソート順になるように'display_order'を設定する
		// ログインユーザのお気に入りショップ・カタログ・カテゴリ
		$this->loadModel('User');
		// TODO ユーザID指定
		$user = $this->User->find('first', array('conditions' => array('User.id' => 1)));
		foreach ($result as $k=>$v){
// 			$result[$k]['display_order'] = 0;
			foreach ($v['CoordinationChannel'] as $key=>$val){
				$result[$k]['CoordinationChannel'][$key]['display_order'] = 0;
				if(in_array($val['Coordination']['shop_id'],explode(',',$user['User']['favorite_shop']))){
// 					$result[$k]['display_order'] +=100;
					$result[$k]['CoordinationChannel'][$key]['display_order'] += 10000;
				}
				if(in_array($val['Coordination']['catalog_id'],explode(',',$user['User']['favorite_catalog']))){
// 					$result[$k]['display_order'] +=100;
					$result[$k]['CoordinationChannel'][$key]['display_order'] += 10000;
				}
				if( count($val['Coordination']['CoordinationCategory']) > 0 &&
					in_array($val['Coordination']['CoordinationCategory'][0]['category_id'],explode(',',$user['User']['favorite_category']))
				){
// 					$result[$k]['display_order'] +=100;
					$result[$k]['CoordinationChannel'][$key]['display_order'] += 10000;
				}
			}
			if(count($v['MyChannel']) > 0){ $result[$k]['CoordinationChannel'][$key]['display_order'] += 10000; }
		}

		foreach ($result as $k=>$v){
			usort($result[$k]['CoordinationChannel'], function($a, $b) {
				if ($a['display_order'] == $b['display_order']) {
					return 0;
				}
				return ($a['display_order'] > $b['display_order']) ? -1 : 1;
			});
		}

		return $result;
	}

/**
 * index method
 *
 * @return json
 */
	public function index() {
		try {

			$Model = 'Channel';
// 			$Model = 'Coordination';
			$this->loadModel($Model);

			$msgConst = Configure::read('USER_IDX_MESSAGE');
			$this->$Model->Behaviors->load('Containable');
// 			$result = $this->Paginator->paginate();

			$params = null;
// 			$params = array('contain' => false);

			// 上記の指定は以下の形式の方がよいか？
// 			$params['contain'] = false;
// 			$params['contain']['CoordinationChannel'] = array('Coordination');
			$params['contain']['CoordinationChannel']['Coordination'] = array('CoordinationCategory');
			$params['contain']['CoordinationChannel']['fields'] = array('DISTINCT coordination_id');

			$result = $this->$Model->find('all', $params);

			// ログイン時、フィルタ設定に応じてソート
			// 期待のソート順になるように'display_order'を仕込んでおく
			// TODO お気に入りとの紐づけ
			foreach ($result as $k=>$v){
				foreach ($v['CoordinationChannel'] as $key=>$val){
					$result[$k]['CoordinationChannel'][$key]['display_order'] = 0;
					// TODO 条件。ショップ・カタログ・カテゴリ
					if($val['Coordination']['shop_id'] == 3){
						$result[$k]['CoordinationChannel'][$key]['display_order'] += 10000;
					}
				}
			}
			foreach ($result as $k=>$v){
				usort($result[$k]['CoordinationChannel'], function($a, $b) {
					if ($a['display_order'] == $b['display_order']) {
						return 0;
					}
					return ($a['display_order'] > $b['display_order']) ? -1 : 1;
				});
			}

			return $this->Common->returnResult($this->request->is('ajax'), $result, $msgConst[1], $msgConst[0]);

		} catch(Exception $e) {
			return $this->Common->returnResult($this->request->is('ajax'), null, $e->getMessage(), INTERNAL_ERROR_CODE);
		}
	}

/**
 * view method
 *
 * @param string $id
 * @return json
 */
	public function view($id = null) {
		try {
			// TODO POSTにできる？

			$msgConst = Configure::read('USER_VIW_MESSAGE');
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$result = $this->User->find('first', $options);

			return $this->Common->returnResult($this->request->is('ajax'), $result, $msgConst[1], $msgConst[0]);

		} catch(Exception $e) {
			return $this->Common->returnResult($this->request->is('ajax'), null, $e->getMessage(), INTERNAL_ERROR_CODE);
		}
	}

}
