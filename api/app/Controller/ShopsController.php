<?php
App::uses('AppController', 'Controller');
/**
 * Shops Controller
 *
 * @property PaginatorComponent $Paginator
 */
class ShopsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator' , 'Flash');

/**
 * beforeFilter method
 *
 * @return void
 */
	public function beforeFilter() {
	    parent::beforeFilter();
	    $this->Auth->allow(
// 	    					'view',
	    					'index',
	    					'add'
	    					);
		$this->autoRender = false;
		$this->response->type('json');
	}

/**
 * index method
 *
 * @return json
 */
	public function index() {
		try {

// 			$Model = 'Shop';
// 			$Model = 'CatalogSeries';
			$Model = 'CoordinationPicture';
			$this->loadModel($Model);

			$msgConst = Configure::read('USER_IDX_MESSAGE');
			$this->$Model->Behaviors->load('Containable');
// 			$result = $this->Paginator->paginate();

			$params = null;
// 			$params = array('contain' => false);
// 			$params = array('contain' => 'Coordination');
// 			$params = array('contain' => 'CatalogSeries');
// 			$params = array('contain' => 'CatalogSeries.keyword');

// 			$params = array('contain' => array(
// 							'CatalogSeries' => array(
// 								'conditions' => array('CatalogSeries.series_url LIKE' => '%03%')
// 							)
// 						)
// 					);
			// 上記の指定は以下の形式の方がよいか？
// 			$params['contain']['CatalogSeries']['conditions'] = array('CatalogSeries.series_url LIKE' => '%03%');
// 			$params['contain']['CatalogSeries']['conditions']['CatalogSeries.series_url LIKE'] = '%03%';
// 			$params['contain'] = false;
			$params['contain'] = 'Coordination';


			$result = $this->$Model->find('all', $params);

			return $this->Common->returnResult($this->request->is('ajax'), $result, $msgConst[1], $msgConst[0]);

		} catch(Exception $e) {
			return $this->Common->returnResult($this->request->is('ajax'), null, $e->getMessage(), INTERNAL_ERROR_CODE);
		}
	}

/**
 * view method
 *
 * @param string $id
 * @return json
 */
	public function view($id = null) {
		try {
			// TODO POSTにできる？

			$msgConst = Configure::read('USER_VIW_MESSAGE');
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$result = $this->User->find('first', $options);

			return $this->Common->returnResult($this->request->is('ajax'), $result, $msgConst[1], $msgConst[0]);

		} catch(Exception $e) {
			return $this->Common->returnResult($this->request->is('ajax'), null, $e->getMessage(), INTERNAL_ERROR_CODE);
		}
	}

}
