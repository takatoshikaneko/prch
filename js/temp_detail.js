;(function(){
	$(window).load(function(){
		var openPhotoSwipe = function( index ) {
			var options = {
				index: index,
				history: true,
				galleryUID:true,
				galleryPIDs:true,
				focus: false,
				loop: false,
				showHideOpacity: true,
				loadingIndicatorDelay: 1000,
				bgOpacity: 0.85,
				showAnimationDuration: 333,
				hideAnimationDuration: 333,
				pinchToClose: true,
				closeOnScroll: true,
				closeOnVerticalDrag: true,
				escKey: true,
				arrowKeys: true,
				tapToClose: false,
				tapToToggleControls: false,
				shareEl: false,
				fullscreenEl: false,
				closeEl: true,
				arrowEl: false,
				captionEl: false,
				counterEl: false,
				spacing: 0.12,
				maxSpreadZoom: 2,
				// getDoubleTapZoom: function (isMouseClick, item) {
				// 	return item.initialZoomLevel;
				// },
				// zoomEl: false,
				// getThumbBoundsFn: function(index) {
				// 	var thumbnail = document.querySelectorAll('.thumbnail')[index];
				// 	var pageYScroll = window.pageYOffset || document.documentElement.scrollTop; 
				// 	var rect = thumbnail.getBoundingClientRect(); 
				// 	return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
				// }
			};
			var items = [
			    {
			        src: 'img/ch_desc_16.png',
			        w: 768,
			        h: 1152
			    },
			    {
			        src: 'img/ch_desc_15.jpg',
			        w: 798,
			        h: 1005
			    },
			    {
			        src: 'img/ch_desc_14.jpg',
			        w: 840,
			        h: 822
			    }
			];
			var pswpElement = document.querySelectorAll('.pswp')[0];
			var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

			gallery.init();

		};

		$('.bx-slider').bxSlider({
			adaptiveHeight: false,
			mode: 'horizontal',
			infiniteLoop: false,
			hideControlOnEnd: true,
			pager: false,
			oneToOneTouch: true,
		});
		$('.bx-slider').find('li').on('click',function(){
			var index = $('.bx-slider').find('li').index(this);

			openPhotoSwipe( index );
		});

		// openPhotoSwipe();
		// $('.bx-slider').on('click',function(){
		// });
	});
})(jQuery,window);