/*
    menu開閉時のページ固定は変更が必要
*/
// tailレイアウト
;(function($) {
	$(function() {
		var pathname = location.pathname;

		var wook_opt = {
			autoResize: true,
            flexibleWidth: 0,
            container: null,
            offset: 0,
            onLayoutChanged: false
		}

		var options = {
            autoResize: true,
            flexibleWidth: 0,
            container: $( document.getElementById('tile-content') ),
            offset: 0,
            onLayoutChanged: false
        };

		var detailOptions = {
            autoResize: true,
            flexibleWidth: 0,
            container: $( document.getElementById('similar-tile-content') ),
            offset: 0,
            onLayoutChanged: false
        };

		switch(pathname){
			case "/":
		   		setTimeout(function(){
		            $(document.getElementById('tile-child')).wookmark(options);
		            $(document.getElementById('similar-tile-child')).wookmark(detailOptions);
		        },10);

		        $(window).on('resize', function() {
		            $(document.getElementById('tile-child')).wookmark(options);
		        	$(document.getElementById('similar-tile-child')).wookmark(detailOptions);
		        });

		        $('.arrows').on('click', function() {
		            setTimeout(function() {
		                $(document.getElementById('tile-child')).wookmark(options);
		            },500);
		        });

		        $('.rec-ch-more-btn').on('click', function() {
		            setTimeout(function() {
		                $(document.getElementById('tile-child')).wookmark(options);
		            },100);
		        });

		        $('.tab').find('li').on('click', function() {
		            setTimeout(function() {
		                $(document.getElementById('tile-child')).wookmark(options);
		            },100);
		        });

		        $('.rec-ch').find('li').on('click', function() {
		            setTimeout(function() {
		                $(document.getElementById('tile-child')).wookmark(options);
		            },100);
		        });
		        setTimeout(function() {
		            $(document.getElementById('tile-child')).wookmark(options);
		        },100);
				break;
			case "/search/result.html":
				//リストごとに適用
				$(".tilescontent ul.tiles").each(function(e, target){

					$(target).imagesLoaded(function() {
						var opt = wook_opt;
						opt.container = $(target).closest(".tilescontent");
						$(target).wookmark(opt);
	                });
				});

				$(window).on('resize', function() {
					$(".tilescontent ul.tiles").each(function(e, target){
						var opt = wook_opt;
						opt.container = $(target).closest(".tilescontent");
						$(target).wookmark(opt);
					});
		        });

		        $('.columnButtonList li').on('click', function() {
		        	setTimeout(function() {
			        	$(".tilescontent ul.tiles").each(function(e, target){
							var opt = wook_opt;
							opt.container = $(target).closest(".tilescontent");
							$(target).wookmark(opt);
						});
		        	},500);
		        });

				break;
		}



	});
})(jQuery);

// bxslider
;(function($) {
	$(function() {
        var option = {
            auto:      true,
            pause:     4000,
            autoHover: false,
            pager:     true
        };
        var detailOption = {
            adaptiveHeight:   false,
            mode:             'horizontal',
            infiniteLoop:     false,
            hideControlOnEnd: true,
            pager:            false,
            oneToOneTouch:    true,
            useCSS:           false
        };

        if($('.bxslider').length != 0){
            $('.bxslider').bxSlider({option});
        }
        if($('.bx-slider').length != 0){
    		$('.bx-slider').bxSlider({detailOption});
        }
	});
})(jQuery);

// login window(detail)
;(function($) {
    $(function() {

        var $search  = $('.search');
        var $header  = $('.l-header');
        var $modalBg = $('.modal-bg');

        $search.find('a').on('click', function() {
            if( !$(this).hasClass('open') ) {
                $search.trigger('openWindow');
            } else {
                $search.trigger('closeWindow');
            }
        });

        $search.on('openWindow', function() {
        	//navを閉じる
        	if($(".menubar").hasClass("open")){
            	$('.l-nav').trigger('navCloseAnimation');
        	}

        	//searchを開く
            $search.find('img').attr('src', '/img/search_btn_on.png');
            $header.find('.login-window').css("display", "block").animate({opacity : "1"});
            $modalBg.css("display", "block").animate({opacity : "1"});
            $search.find('a').addClass('open');
        });

        $search.on('closeWindow', function() {
            $search.find('img').attr('src', '/img/search_btn_off.png');
            $header.find('.login-window').animate({opacity : "0"}).css("display", "none");
            $modalBg.animate({opacity : "0"}).css("display", "none");
            $search.find('a').removeClass('open');
        });
    });
})(jQuery);

// menu window
;(function($,window) {
    $(function(e) {
        var $main        = $('.l-main');
        var $registerBtn = $('');
        var $footer      = $('.l-footer');
        var $nav         = $('.l-nav');
        var $menubar     = $('.menubar');
        var $content     = $(document.getElementById('content'));
        var $modalBg     = $('.modal-bg');

        var animSpeed    = 250;
        var posTop       = "";
        var footTop      = "";
        var navMarginTop = 0;

        //メディアクエリのmin-widthをオーバーしているかどうかの判定
        //true:オーバーしている false:オーバーしていない
        var isOverMinWidth = function(){
        	if(window.matchMedia('(min-width:970px)').matches){
        		return true;
        	}else{
        		return false;
        	}
        }

        //ナビのCSS更新
        var setNavStyle = function(){
        	var navH = $nav.height();
    		var navTop = Number($nav.css("top").replace("px", ""))	;
    		navMarginTop = -1 * (navH + navTop);

        	if(!$menubar.hasClass("open")){
	        	if(isOverMinWidth()){
	        		$nav.css({
	        			marginTop:navMarginTop
	        		});
	        	}else{
	        		$nav.css({
	        			marginTop:0
	        		});
	        	}
        	}else{
        		if(isOverMinWidth()){
        			$nav.css({
	        			left:0
	        		});
        		}else{
        			$nav.css({
	        			left:"40%"
	        		});
        		}
        	}
        }

		setNavStyle();
        $(window).on('resize', function() {
        	setNavStyle();
        });


        $menubar.on('click', function() {

            // menubar animation
            if( $(this).hasClass('close') ) {
                $nav.trigger('navOpenAnimation');
                $nav.trigger('noScroll');
            } else {
                $nav.trigger('navCloseAnimation');
                $nav.trigger('onScroll');
            }

            // menubar change image
            if( $(this).hasClass('close') ) {
                $(this).find('img').attr('src', '/img/header_menubar_close_btn.png');
            } else {
                $(this).find('img').attr('src', '/img/header_menubar_open_btn.png');
            }

        });

        $nav.find('li:first-child').on('click', function() {
            $nav.trigger('navCloseAnimation');
            $nav.trigger('onScroll');
            $menubar.find('img').attr('src', '/img/header_menubar_close_btn.png');
            $menubar.removeClass('open').addClass('close');
        });

        // open menu
        $nav.on('navOpenAnimation', function( ) {
        	//search close
        	//navを閉じる
        	if($(".login-window").css("opacity") > 0){
            	$('.search').trigger('closeWindow');
        	}

        	//nav open
            posTop = $(window).scrollTop();
            $(window).scrollTop( posTop );

            $modalBg.css("display", "block").animate({opacity : "1"});
            $content.stop().animate({left: '-60%'},animSpeed);

            setTimeout(function() {
                $main.css({
                    "position" : "fixed",
                    "top" : -posTop
                });
            },10);

        	// width >= min-width
        	if(isOverMinWidth()){
        		$nav.stop().animate({marginTop: "0px"},animSpeed);
        	}
        	// width < min-width
        	else{
                $nav.stop().animate({left: '40%'},animSpeed);
        	}

            $menubar.removeClass('close').addClass('open');

        });

        // close menu
        $nav.on('navCloseAnimation', function() {
            $main.css({
                "top"      : "",
                "position" : "relative"
            });
            setTimeout(function() {
                $(window).scrollTop(posTop);
            },0);
            $modalBg.animate({opacity : "0"}).css("display", "none");
            $content.stop().animate({left: '0%'},animSpeed);

            // width >= min-width
        	if(isOverMinWidth()){
        		$nav.stop().animate({marginTop: navMarginTop},animSpeed);
        	}
        	// width < min-width
        	else{
                $nav.stop().animate({left: '100%'},animSpeed);
        	}

        	$menubar.removeClass('open').addClass('close');

        });

        $nav.on('noScroll', function() {
            // pc & sp
            var scroll_event = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';
            $(document).on(scroll_event,function(e){e.preventDefault();});
            $(document).on('touchmove.noScroll', function(e) {e.preventDefault();});
        });

        $nav.on('onScroll', function() {
            // pc & sp
            var scroll_event = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';
            $(document).off(scroll_event);
            $(document).off('.noScroll');
        });
    });
})(jQuery,window);

// photo swipe
;(function($) {
    $(function() {
        var pswpElement = document.querySelectorAll('.pswp')[0];
        var options     = {
            history: true,
            galleryUID:false,
            galleryPIDs:false,
            focus: false,
            loop: false,
            showHideOpacity: true,
            loadingIndicatorDelay: 1000,
            bgOpacity: 0.85,
            showAnimationDuration: 333,
            hideAnimationDuration: 333,
            pinchToClose: true,
            closeOnScroll: true,
            closeOnVerticalDrag: true,
            escKey: true,
            arrowKeys: true,
            tapToClose: false,
            tapToToggleControls: false,
            shareEl: false,
            fullscreenEl: false,
            closeEl: true,
            arrowEl: false,
            captionEl: false,
            counterEl: false,
            spacing: 0.12,
            maxSpreadZoom: 2
        }
        var items = [
            {
                src: '/img/ch_desc_16.png',
                w: 640,
                h: 855
            },
            {
                src: '/img/ch_desc_16.png',
                w: 640,
                h: 855
            },
            {
                src: '/img/ch_desc_16.png',
                w: 640,
                h: 855
            }
        ];

        $('.bx-slider').find('img').on('click', function() {
            new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options ).init();
        });
    });
})(jQuery);

// favolite btn
;(function($) {
    $(function() {
        $('.favolite-btn').on('click', function() {
            if( $(this).hasClass('on') ) {
                $(this).removeClass('on');
            } else {
                $(this).addClass('on');
            }
        });
    });
})(jQuery);

// detail msg
;(function($) {
    $(function() {
        $('.tooltipBtn').on('click', function() {
            if( $(this).hasClass('on') ) {
                $(this).removeClass('on');
                $(this).next().removeClass('on');
            } else {
                $(this).addClass('on');
                $(this).next().addClass('on');
            }
        });
    });
})(jQuery);

// 会員登録はこちら追従

;(function($,windows) {
    $(function() {
        var $triggerNode = $('.login-before.page-transition-btn');
        if($triggerNode.length > 0){
            $(window).on('scroll', function() {
                var triggerNodePosition = $triggerNode.offset().top - $(window).height();
                if( $(window).scrollTop() > triggerNodePosition ) {
                    $triggerNode.find("a").css("position", "relative");
                } else {
                    $triggerNode.find("a").css({
                        "position"    : "fixed"
                    });
                }
            });

        }
    });
})(jQuery,window);

// top カラム変更
;(function($,window) {
    $(function() {
        $('.arrows').on('click', function(e) {
            e.preventDefault();
            if( $(this).hasClass('on') ) {
                $(this).removeClass('on');
                $(this).find('img').trigger("chgImg", "./img/header_btn_col1.png");
                $(this).trigger('chgColumn');
            } else {
                $(this).addClass('on');
                $(this).find('img').trigger("chgImg", "./img/header_btn_col2.png");
                $(this).trigger('chgColumn');
            }
        });

        // change images
        $(document).on('chgImg', '.arrows', function() {
            if( $(this).hasClass('on') ) {
                $(this).find('img').attr('src', './img/header_btn_col1.png');
            } else {
                $(this).find('img').attr('src', './img/header_btn_col2.png');
            }
        });

        // change 1column
        $(document).on('chgColumn', '.arrows', function() {
            if( $(this).hasClass('on') ) {
                $('.l-main .rec-ch li').animate({"opacity" : "0"}, function() {
                    $(this).animate({"opacity" : "1"}).addClass("col1");
                });

                $('.tiles li').animate({"opacity" : "0"}, function() {
                    $(this).animate({"opacity" : "1"}).addClass("col1");
                });
            } else {
                $('.l-main .rec-ch li').animate({"opacity" : "0"}, function() {
                    $(this).animate({"opacity" : "1"}).removeClass("col1");
                });

                $('.tiles li').animate({"opacity" : "0"}, function() {
                    $(this).animate({"opacity" : "1"}).removeClass("col1");
                });
                // $('.tiles li').removeClass('col1');
            }
        });
    });
})(jQuery,window);

//saerch result カラム変更
;(function($,window) {
    $(function() {
    	$(".columnButtonList li").on("click", function(e){
    		e.preventDefault();

			$(".columnButtonList li").removeClass("cur");

			var idx = $(".columnButtonList li").index($(this));
			$(".columnButtonList li").eq(idx).addClass("cur");

			var $target = $(".tilescontent ul.tiles > li");
    		switch(idx){
	    		case 0:
	    			$target.animate({"opacity" : "0"}, function() {
	                    $(this).animate({"opacity" : "1"}).addClass("col1");
	                });
	    			break;
	    		case 1:
	    			$target.animate({"opacity" : "0"}, function() {
	                    $(this).animate({"opacity" : "1"}).removeClass("col1");
	                });
	        		break;
    		}
    	});

    });
})(jQuery,window);




// tab animation
;(function() {
    $(function() {
        var left      = 'tab-relax';
        var center    = 'tab-cute';
        var right     = 'tab-casual';
        var moveCount = 0;

        // tabのリセット
        $(document).on('resetTabClass', function() {
            moveCount = 0;
            $('.tab-relax').addClass('left');
            $('.tab-cute').addClass('center');
            $('.tab-casual').addClass('right');
        });

        // tab classリセット
        $(document).on('allDeleteTabClass', function() {
            $('.center').removeClass('center');
            $('.left').removeClass('left');
            $('.right').removeClass('right');
        });

        $(document).trigger('resetTabClass');

        // ボタン押下時tabclass変更
        $(document).on('chgTabClass', function(e, $center, rClass) {
            $('.'+$center).removeClass(rClass).addClass('center');
            $('.'+$center).next().addClass('right');
            $('.'+$center).prev().addClass('left');
        });

        // おすすめチャンネルクリック後のtabclass変更
        $(document).on('recChgTabClass', function(e, target) {

        	var $liList = $(".tab:eq(0)").find("li");
        	var liCnt = $liList.length;
        	var idx = $liList.index($(target));

        	$(target).addClass('center');

			if(idx == 0){
				$(target).closest("ul").find("li:last").addClass("left");
	            $('.center').next().addClass('right');
			}else if(idx == liCnt -1){
				$('.center').prev().addClass('left');
				$(target).closest("ul").find("li:first").addClass("right");
			}else{
	            $('.center').prev().addClass('left');
	            $('.center').next().addClass('right');
			}
        });

        // スライド(アニメーションなし)
        $(document).on('noAnimateSlide', function() {
        	var $liList = $(".tab:eq(0)").find("li");
        	var liCnt = $liList.length;
        	var idx = $liList.index($(".center"));

        	$(".tab").each(function(){
            	if(idx == 0){
            		$lis = $(this).find("li:last").clone(true);
            		$lis.insertBefore($(this).find("li:first"));
            		$(this).find("li:last").remove();
            		$(this).css({left:0});
            	}else{
            		$lis = $(this).find(".left").prevAll().clone(true);
            		$lis.insertAfter($(this).find("li:last"));
            		$(this).find(".left").prevAll().remove();
            		$(this).css({left:0});
            	}
        	});

        })

        // スライドアニメーション
        $(document).on('animate', function(e, control) {
        	$('.tab').find('ul').animate(
            		{'left' : '11.1111' * moveCount + '%'},
            		{
            			complete:function(){
            				if(control == "right"){
                				// 最後のliを先頭に移動
    							var $li = $(this).find("li:first").clone(true);
    							$li.insertAfter($(this).find("li:last"));
    							$(this).find("li:first").remove();
    							$(this).css({left:0});
            				}else{
                				// 最後のliを先頭に移動
    							var $li = $(this).find("li:last").clone(true);
    							$li.insertBefore($(this).find("li:first")).addClass("left");
    							$(this).find("li:last").remove();
    							$(this).css({left:0});
            				}
            			}
            		}
            );
        });

        // リセットアニメーション
        $(document).on('resetAnimate', function() {
            $('.tab').find('ul').css('left' , '0%');
        });

        $('.tab').find('li').on('click', function(e) {
            e.preventDefault();

            // 右ボタン押下時
            if( $(this).hasClass('right') ) {
                var $center = $(this).get(0).className.split(" ")[0];
                $(document).trigger('allDeleteTabClass');
                $(document).trigger('chgTabClass', $center, 'right');

                moveCount = -1;
                $(document).trigger('animate', "right");
            }

            // 中央ボタン押下時
            if( $(this).hasClass('center') ) { }

            // 左ボタン押下時
            if( $(this).hasClass('left') ) {
                var $center = $(this).get(0).className.split(" ")[0];
                $(document).trigger('allDeleteTabClass');
                $(document).trigger('chgTabClass', $center, 'left');

                moveCount = 1;
                $(document).trigger('animate', "left");
            }
        });

        // backボタン
        $('.tab-btn.back').on('click', function() {
            if( $('.tab-cute').hasClass('left') ) {
                return;
            }

            var $center = $('.left').get(0).className.split(" ")[0];
            $(document).trigger('allDeleteTabClass');
            $(document).trigger('chgTabClass', $center, 'left');

            moveCount = 1;
            $(document).trigger('animate', "left");
        });

        // nextボタン
        $('.tab-btn.next').on('click', function() {
            if( $('.tab-relax').hasClass('right') ) {
                return;
            }

            var $center = $('.right').get(0).className.split(" ")[0];
            $(document).trigger('allDeleteTabClass');
            $(document).trigger('chgTabClass', $center, 'right');

            moveCount = -1;
            $(document).trigger('animate', "right");
        });

        // チャンネル一覧ボタン押下後、tabに遷移しクリックしたチャンネルにtabを変更
        $('.rec-ch').find('li').on('click', function(e) {
            e.preventDefault();
            var $center     = $('.center');
            var targetClass = $(this).get(0).className.split(" ")[0];
            var targetOfs   = $('.tab').offset().top;
            $('html,body').animate({scrollTop: targetOfs - 50}); // 一覧へ遷移

            // tabのリセット
            $(document).trigger('resetTabClass');
            $(document).trigger('allDeleteTabClass');
            $(document).trigger('resetAnimate');

            switch( $(this).find('img').attr('alt') ) {
	            case "キュート" :
	                moveCount = 0;
	                $(document).trigger('recChgTabClass', '.tab-cute');
	                $(document).trigger('noAnimateSlide');
	                break;
                case "カジュアル" :
                    moveCount = 0;
                    $(document).trigger('recChgTabClass', '.tab-casual');
                    $(document).trigger('noAnimateSlide');
                    break;
                case "ビジネス" :
                    moveCount = 1;
                    $(document).trigger('recChgTabClass', '.tab-business');
                    $(document).trigger('noAnimateSlide');
                    break;
                case "インテリア" :
                    moveCount = 2;
                    $(document).trigger('recChgTabClass', '.tab-interior');
                    $(document).trigger('noAnimateSlide');
                    break;
                case "ブラック" :
                    moveCount = 3;
                    $(document).trigger('recChgTabClass', '.tab-black');
                    $(document).trigger('noAnimateSlide');
                    break;
                case "モード" :
                    moveCount = 4;
                    $(document).trigger('recChgTabClass', '.tab-mode');
                    $(document).trigger('noAnimateSlide');
                    break;
                case "ハッピー" :
                    moveCount = 5;
                    $(document).trigger('recChgTabClass', '.tab-happy');
                    $(document).trigger('noAnimateSlide');
                    break;
                case "リラックス" :
                    moveCount = 5;
                    $(document).trigger('recChgTabClass', '.tab-relax');
                    $(document).trigger('noAnimateSlide');
                    break;
            }
        });
    });
})(jQuery,window);

//top json取得
;(function($) {
    $(function() {

        $(document).on('getJson', function(e, url, callbackSuccess, target){
        	$.ajaxSetup({
    		  cache: false
    		});
            $.getJSON(url, function(json) {

            }).success(function(json) {
            	callbackSuccess(json, target);

            }).error(function(jqXHR, textStatus, errorThrown) {
                console.log("エラー:" + textStatus);
                console.log("テキスト:" + jqXHR.responseText);
            }).complete(function() {
            });
        });

        var targetClass = "tab-cute";
        var loadImgCnt = 4;
        var itemList    = loadImgCnt;
        var curItemList = 0;


    	switch(location.pathname){
		case "/":
	        var url = "/json/topItem.json";
	        var callbackSuccess = function(json, target){
	        	var viewItems = 0;
                $('.tiles').find('li').remove();

                for(var i = 0; i < json.items.length; i++) {
                    if( json.items[i]['cate'] == targetClass && viewItems < itemList ) {
                        $('<li><a href=""><img src="' + json.items[i]['img'] + '" alt=""></a></li>').appendTo('.tiles').hide().fadeIn(1000);
                        viewItems++;
                    }
                }

                var $item = $(document.getElementById('tile-child'));
                $item.imagesLoaded(function() {
                    var options = {
                            autoResize: true,
                            flexibleWidth: 0,
                            container: $( document.getElementById('tile-content') ),
                            offset: 0,
                            onLayoutChanged: false

                    };
                    $item.wookmark(options);
                });
	        }

	        $('.tab').find('li').on('click', function() {
	            itemList = loadImgCnt;
	            targetClass = $(this).get(0).className.split(" ")[0];
	            $(document).trigger('getJson', [url, callbackSuccess]);
	        });

	        $('.rec-ch').find('li').on('click', function() {
	            itemList = loadImgCnt;
	            targetClass = $(this).get(0).className.split(" ")[0];
	            targetClass = targetClass.replace(/rec-/g, 'tab-');
	            $(document).trigger('getJson', [url, callbackSuccess]);
	        });

	        $('.rec-ch-more-btn').on('click', function(e) {
	            e.preventDefault();
	            curItemList = $('.tiles').find('li').length;
	            itemList += itemList
	            $(document).trigger('getJson', [url, callbackSuccess]);
	        });

	        $(document).trigger('getJson', [url, callbackSuccess]);

	        break;
		case "/search/result.html":
	        var url = "/json/topItem.json";
	        var callbackSuccess = function(json, target){
	        	var viewItems = 0;

                $(target).find('li').remove();

                for(var i = 0; i < json.items.length; i++) {
                    if( json.items[i]['cate'] == targetClass && viewItems < itemList ) {
                        $('<li><a href=""><img src="' + json.items[i]['img'] + '" alt=""></a></li>').appendTo($(target)).hide().fadeIn(1000);
                        viewItems++;
                    }
                }

                $(target).imagesLoaded(function() {
                    var options = {
                            autoResize: true,
                            flexibleWidth: 0,
                            container: $(target).closest(".tilescontent"),
                            offset: 0,
                            onLayoutChanged: false

                    };
                    $(target).wookmark(options);
                });
	        }


	        $('.similar-list-more').on('click', function(e) {
	            e.preventDefault();
	            $target = $(this).prev().find(".tilescontent .tiles");
	            curItemList = $target.find('li').length;
	            itemList = curItemList +loadImgCnt;
	            $(document).trigger('getJson', [url, callbackSuccess, $target]);
	        });


			break;
		}
    });
})(jQuery,window);

//チェックボタン処理
$(function(){
    $(".entryTable .check").click(function(){
    	if($(this).closest("tr").hasClass("select")){
        	if($(this).hasClass("cur")){
            	$(this).closest(".entryTable").find("span.check").removeClass("cur");
        	}else{
            	$(this).closest(".entryTable").find("span.check").addClass("cur");
        	}
    	}else{
            if( $(this).hasClass('cur') ) {
    	        $(this).removeClass('cur');
    	    }else{
    	        $(this).addClass('cur');
        	};
    	}
    });

	$(".caption dt").click(function(){
	    $(this).children().toggleClass("cur");
        $(this).closest('dl').find("dd").slideToggle();
	});

	$("#entry .entryTable .searchTable .select").click(function(){
        $(this).children().children().children().toggleClass('cur');
        $(this).siblings().slideToggle();
	});


});
