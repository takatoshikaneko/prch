var CSSTransitionGroup = React.addons.CSSTransitionGroup;
var ReactTransitionGroup = React.addons.TransitionGroup;
var APP = {};
var TopItem =
{
    col   : 2,
    items : [
        {
            type : 'images',
            favolite : false,
            link : '#',
            img  : './img/ch_desc_16.png',
        },
        {
            type : 'images',
            favolite : false,
            link : '#',
            img  : './img/ch_desc_01.png',
        },
        {
            type : 'images',
            favolite : false,
            link : '#',
            img  : './img/ch_desc_02.jpg',
        },
        {
            type : 'images',
            favolite : false,
            link : '#',
            img  : './img/ch_desc_03.png',
        },
        {
            type : 'images',
            favolite : false,
            link : '#',
            img  : './img/ch_desc_04.png',
        },
        {
            type : 'images',
            favolite : false,
            link : '#',
            img  : './img/ch_desc_05.jpg',
        },
        {
            type : 'images',
            favolite : false,
            link : '#',
            img  : './img/ch_desc_06.jpg',
        }
    ]
};

//migration必要
if( location.hash === '#login' ){
    var TopTabItem = {
        current : 'casual',
        items : [
            // { name : 'my', text : 'あなたにオススメ' },
            { name : 'cute', text : 'キュート' },
            { name : 'casual', text : 'カジュアル' },
            { name : 'business', text : 'ビジネス' },
            { name : 'interior', text : 'インテリア' },
            { name : 'black', text : 'ブラック' },
            { name : 'mode', text : 'モード' },
            { name : 'happy', text : 'ハッピー' },
            { name : 'relax', text : 'リラックス' }
        ]
    };
}else{
    var TopTabItem = {
        current : 'casual',
        items : [
            { name : 'cute', text : 'キュート' },
            { name : 'casual', text : 'カジュアル' },
            { name : 'business', text : 'ビジネス' },
            { name : 'interior', text : 'インテリア' },
            { name : 'black', text : 'ブラック' },
            { name : 'mode', text : 'モード' },
            { name : 'happy', text : 'ハッピー' },
            { name : 'relax', text : 'リラックス' }
        ]
    };
}


var TopChannel = {
    channel : [
        {
            link  : '#',
            image : './img/ch_cute.jpg',
            text  : 'キュート'
        },
        {
            link  : '#',
            image : './img/ch_casual.jpg',
            text  : 'カジュアル'
        },
        {
            link  : '#',
            image : './img/ch_business.jpg',
            text  : 'ビジネス'
        },
        {
            link  : '#',
            image : './img/ch_interior.jpg',
            text  : 'インテリア'
        },
        {
            link  : '#',
            image : './img/ch_black.jpg',
            text  : 'ブラック'
        },
        {
            link  : '#',
            image : './img/ch_mode.jpg',
            text  : 'モード'
        },
        {
            link  : '#',
            image : './img/ch_happy.jpg',
            text  : 'ハッピー'
        },
        {
            link  : '#',
            image : './img/ch_relax.jpg',
            text  : 'リラックス'
        },

    ]
};

//migration必要
if( location.hash === '#login' ){
    var MenuList = {
        list : [
            // { link : false, text : 'menu' },
            { link : '#', text : 'トップページ' },
            { link : '#', text : 'ログアウト' },
            { link : '#', text : 'Myチャンネル' },
            { link : '#', text : 'お気に入りリスト' },
            { link : '#', text : 'カテゴリから探す' },
            { link : '#', text : 'ショップから探す' },
            { link : '#', text : 'カタログから探す' },
            { link : '#', text : '注目ワードから探す' },
            { link : '#', text : 'マイページ' },
            { link : '#', text : 'ヘルプ' },
        ]
    };
}else{
    var MenuList = {
        list : [
            // { link : false, text : 'menu' },
            { link : '#', text : 'トップページ' },
            { link : '#', text : 'ログイン' },
            { link : '#', text : '新規会員登録' },
            { link : '#', text : 'ヘルプ' },
        ]
    };
}



var DetailInfo = {
    favolite : true,
    MainVisual  : [],
    ProductCost : [
        { 'title' : 'タートルネックハイゲージニット', cost : '4,500' },
        { 'title' : 'ウールスカート', cost : '5,800' },
        { 'title' : 'トラッドコード', cost : '12,000' },
    ],
    Catalog     : {
        'title' : 'ベルメゾンファッション冬号 P.112'
    },
    Similarity  : [
        { image : './img/detail_similar_01.jpg' },
        { image : './img/detail_similar_02.jpg' },
        { image : './img/detail_similar_03.png' },
        { image : './img/detail_similar_04.jpg'},
        { image : './img/detail_similar_05.jpg' },
        { image : './img/detail_similar_06.jpg' }
    ],
    Word        : [
        { text : 'ニット' },
        { text : '白スカート' },
        { text : '冬コード' },
        { text : 'バッグ' },
        { text : 'ベルメゾン' },
        { text : '無地' },
        { text : '赤' },
        { text : '冬小物' },
        { text : '春' },
        { text : 'パンプス' },
    ]
};

var LoginKeyword = ["ニット","白スカート","冬コート","バッグ","ベルメゾン","無地","冬小物","春","パンプス"];

//migration必要
var DetailMainVisualConf = {
    images : [
        {
            src : './img/ch_desc_16.png',
            w: 768,
            h: 1152
        },
        {
            src : './img/ch_desc_15.jpg',
            w: 798,
            h: 1005
        },
        {
            src : './img/ch_desc_14.jpg',
            w: 840,
            h: 822
        }
    ],
    photoSwipeOptions : {
        history: true,
        galleryUID:false,
        galleryPIDs:false,
        focus: false,
        loop: false,
        showHideOpacity: true,
        loadingIndicatorDelay: 1000,
        bgOpacity: 0.85,
        showAnimationDuration: 333,
        hideAnimationDuration: 333,
        pinchToClose: true,
        closeOnScroll: true,
        closeOnVerticalDrag: true,
        escKey: true,
        arrowKeys: true,
        tapToClose: false,
        tapToToggleControls: false,
        shareEl: false,
        fullscreenEl: false,
        closeEl: true,
        arrowEl: false,
        captionEl: false,
        counterEl: false,
        spacing: 0.12,
        maxSpreadZoom: 2,
    }
};

var ModalContent = React.createClass({
    getInitialState : function(){
        return {
            Modal : {
                status : false
            }
        }
    },
    componentDidMount : function(){
        $( APP ).on('toggleModal',function(e){
            var status = this.state.Modal.status ? false : true;
            this.setState({
                Modal  : {
                    status : status
                }
            });
        }.bind(this));
    },
    componentDidUpdate : function(){
        if( this.state.Modal.status ){
            $( this.getDOMNode() ).css('display','block').animate({opacity:0.8});
        }else{
            $( this.getDOMNode() ).animate({opacity:0},function(){
                $(this).css('display','none');
            });
        }
    },
    onClickModal : function(){
        //メニュークリック時にAppPrchのStateを変更しメニュー開閉
        $(APP).trigger('toggleModal').trigger('toggleSearch');
    },
    render : function(){
        return (
            <div onClick={this.onClickModal} className="modal-bg"></div>
        )
    }
});

var ModalMenuContent = React.createClass({
    getInitialState : function(){
        return {
            Modal : {
                status : false
            }
        }
    },
    componentDidMount : function(){
        $( APP ).on('toggleModalMenu',function(e){
            var status = this.state.Modal.status ? false : true;
            this.setState({
                Modal  : {
                    status : status
                }
            });
        }.bind(this));
    },
    componentDidUpdate : function(){
        if( this.state.Modal.status ){
            $( this.getDOMNode() ).css('display','block').animate({opacity:0.8});
        }else{
            $( this.getDOMNode() ).animate({opacity:0},function(){
                $(this).css('display','none');
            });
        }
    },
    render : function(){
        return (
            <div className="modal-bg"></div>
        )
    }
});

//メニュー
var Menu    = React.createClass({
    getInitialState : function(){
        return MenuList;
    },
    componentDidUpdate: function(callback) {
        var par = this.props.Menu.status ? '40%' : '100%';
        $( this.getDOMNode() ).animate({ 'left' : par },250);
    },
    onClickMenu : function(e){
        e.preventDefault();
        //メニュークリック時にAppPrchのStateを変更しメニュー開閉
        if( !$( document.getElementById('content') ).filter(':animated').length ){
            $(APP).trigger('toggleMenu').trigger('toggleModalMenu');
        }
    },
    render:function(){
        return (
            <div className="l-nav">
                <nav>
                    <ul>
                        <li onClick={this.onClickMenu}>Menu</li>
                        {this.state.list.map(function(value,index){
                            var dom = value.link ? <a href={value.link}>{value.text}</a> : value.text;
                            return <li key={index}>{dom}</li>;
                        })}
                    </ul>
                </nav>
            </div>
        )
    }
});

var HeaderSearch = React.createClass({
    getInitialState : function(){
        return {
            Search : {
                status : false
            },
            keyword : LoginKeyword
        }
    },
    componentDidMount : function(){
        $( APP ).on('toggleSearch',function(e){
            var status = this.state.Search.status ? false : true;
            this.setState({
                Search  : {
                    status : status
                }
            });
        }.bind(this));
    },
    componentDidUpdate: function(callback) {

        if( this.state.Search.status ){
            $( this.getDOMNode() ).css('display','block').animate({opacity:1});
        }else {
            $( this.getDOMNode() ).animate({opacity:0},function(){
                $(this).css('display','none');
            });
        }
    },
    render : function(){
        if( !this.props.App.login ){
            return(
                <div className="login-window">
                    <div className="login-before">
                        <p>検索はログインしてから<br />お使い頂けます。</p>
                        <ul>
                            <li><div className="page-transition-btn"><p><a href="#">ログイン</a></p></div></li>
                            <li><div className="page-transition-btn"><p><a href="#">新規会員登録</a></p></div></li>
                        </ul>
                    </div>
                </div>
            )
        }else{
            return (
                <div className="login-window">
                    <div className="login-after">
                        <h2>キーワードから探す</h2>
                        <form action="#" method="GET">
                            <input type="text" placeholder="例) カーディガン" />
                            <input type="submit" value="検索" />
                        </form>
                        <h2>注目のワードから探す</h2>
                        <ul className="attn-word">
                            {this.state.keyword.map(function(value,index){
                                return <li><a href="#">#{value}</a></li>
                            })}
                        </ul>

                        <ul className="choose-btn">
                            <li><div className="page-transition-btn"><a href="#">注目ワード一覧</a></div></li>   
                            <li><div className="page-transition-btn"><a href="#">カテゴリから選ぶ</a></div></li>
                            <li><div className="page-transition-btn"><a href="#">ショップから選ぶ</a></div></li>
                            <li><div className="page-transition-btn"><a href="#">カタログから選ぶ</a></div></li>
                        </ul>
                    </div>
                </div>
            )
        }
    }
});

//コンテンツ部分ヘッダー
var ContentHeader = React.createClass({
    getInitialState : function(){
        //下記はContentTopItemsで本来持つべき
        //migration 必要
        return {
            col : 2,
            search : false
        }
    },
    componentDidMount : function(){
        $('h1').children('a').on('click',function(e){
            e.preventDefault();
            $('html, body').animate({ scrollTop: 0 });
        });
    },
    onClickMenu : function(e){
        e.preventDefault();
        //メニュークリック時にAppPrchのStateを変更しメニュー開閉
        if( !$( document.getElementById('content') ).filter(':animated').length ){
            $(APP).trigger('toggleMenu').trigger('toggleModalMenu');
        }
        //migration 必要
        if( $('.login-window').css('display') === 'block' ){
            $(APP).trigger('toggleModal').trigger('toggleSearch');
        }
    },
    onClickSearch : function(e){
        e.preventDefault();
        //メニュークリック時にAppPrchのStateを変更しメニュー開閉
        this.state.search = this.state.search === false ? true : false;
        this.setState({
            search : this.state.search
        });
        $(APP).trigger('toggleModal').trigger('toggleSearch');
    },
    onChangeRoute : function(e){
        e.preventDefault();
        //戻るアイコンクリック時にAppPrchのStateを変更
        $(APP).trigger('changeRoute',['Top']).trigger('DetailDisplayTop',['Top']).trigger('fireScrollPos');
    },
    clickChangeCol : function(e){
        e.preventDefault();
        $(APP).trigger('toggleItemCol');
        this.state.col = this.state.col === 1 ? 2 : 1;
        this.setState({
            col : this.state.col
        });
    },
    transitionHref : function(e){
        e.preventDefault();
        var hash = this.props.App.login ? '#' : '#login';
        window.location.href = hash;
        window.location.reload();
    },
    render:function(){
        var leftDom = '';
        if(this.props.Route === 'Top'){
            var imagePath = "./img/header_btn_col" + this.state.col + ".png"
            leftDom = <div className="arrows"><a href="#"><img src={imagePath} alt="" onClick={this.clickChangeCol.bind(this)} /></a></div>;
        }else{
            leftDom = <div className="arrows"><a href="#" onClick={this.onChangeRoute}><img src="./img/header_btn_back.png" alt="" className="back"/></a></div>
        }
        var login = this.props.App.login ? 'ログアウト' : 'ログイン' ;
        var loginClass = this.props.App.login ? 'login status-logout' : 'login status-login' ;

        var menubarDom = '';
        if( this.props.Menu.status ) {
            menubarDom = <div className="menubar open"><a href="#" onClick={this.onClickMenu}><img src="./img/header_menubar_open_btn.png" alt="" /></a></div>;
        } else {
            menubarDom = <div className="menubar close"><a href="#" onClick={this.onClickMenu}><img src="./img/header_menubar_close_btn.png" alt="" /></a></div>;
        }

        var searchPath = '';
        if( this.state.search ) {
            searchPath = './img/search_btn_on.png';
        } else {
            searchPath = './img/search_btn_off.png'; 
        }

        return (
            <div className="l-header">
                {leftDom}
                <div className="search"><a href="#"><img onClick={this.onClickSearch} src={searchPath} alt="" aria-hidden="true" /></a></div>
                <h1><a href="#">Paraly Channel</a></h1>
                <div className="login-area">
                    <div className={loginClass}><a href="#" onClick={this.transitionHref.bind(this)}>{login}</a></div>
                    <HeaderSearch key="header-search" App={this.props.App} Menu={this.props.Menu} />
                </div>
                {menubarDom}
            </div>
        )
    }
});

//トップページメインVisual
var ContentTopMainVisual = React.createClass({
    getInitialState : function(){
        return {
            status : true
        };
    },
    componentDidMount : function() {
        var bxParam = {
            auto: true,
            pause: 4000,
            autoHover: false,
            pager: true,
        }

        $('.bxslider').bxSlider(bxParam);
    },
    clickClose : function(){
        this.setState({
            status : false
        });
    },
    render : function(){
        if( this.state.status ){
            return(
                <div className="guide">
                    <ul className="bxslider" id="feed">
                        <li><img src="./img/guide.jpg" alt="" /></li>
                        <li><img src="./img/guide.jpg" alt="" /></li>
                        <li><img src="./img/guide.jpg" alt="" /></li>
                    </ul>
                    {/*<div className="guide-close" onClick={this.clickClose}></div>*/}
                </div>
            );
        }else{
            return <div className="guide"></div>;
        }

    }
});

//トップページチャンネル一覧
var ContentTopChannel = React.createClass({
    getInitialState : function(){
        return TopChannel;
    },
    //コンテンツがrenderされた後1度だけ呼び出される
    componentDidMount : function(){
        $( APP ).on('toggleItemCol',function(e){
            var status = this.state.col === 1 ? 2 : 1;
            //全体を一旦透過その後再度表示
            $(document.getElementById('rec-child')).animate({opacity:0},function(){
                $(this).delay(100).animate({opacity:1});
            });
            //stateを更新
            setTimeout(function(){
                this.setState({
                    col  : status
                });
            }.bind(this),400);
        }.bind(this));
    },
    render : function(){
        var colClass = this.state.col === 1 ? 'col1' : '';
        return (
            <div className="rec-ch">
                <h2>あなたにオススメのチャンネル</h2>
                <ul id="rec-child">
                    {this.state.channel.map(function(value,index){
                        return (
                            <li key={index} className={colClass}>
                                <a href={value.link}><img src={value.image} alt={value.text} /></a>
                                <p><span className="rec-ch-bg">{value.text}</span></p>
                            </li>
                        )
                    })}
                </ul>
                <div className="page-transition-btn"><p><a href="#">チャンネル一覧</a></p></div>
            </div>
        )
    }
});

//トップページチャンネルスワイプ部分タブ
var ContentChannelTab = React.createClass({
    getInitialState : function(){
        return TopTabItem;
    },
    clickTab : function(name){
        this.setState({
            current : name
        });
    },
    render:function(){
        return (
            <div className={ 'rec-ch-tabmenu ' + this.state.current}>
                <div className="tab-wrap">
                    <div className="back tab-btn"><img src="./img/tab_back_btn.png" alt="" /></div>
                    {this.state.items.map(function(value,index){
                        var tabClass = this.state.current === value.name ? 'tab-' + value.name + ' on' : 'tab-' + value.name;
                        return <span onClick={this.clickTab.bind(this,value.name)} className={tabClass} key={index}>{value.text}</span>
                    }.bind(this) )}
                    <div className="next tab-btn"><img src="./img/tab_next_btn.png" alt="" /></div>
                </div>
            </div>
        )
    }
}); 

//トップページアイテム
var ContentTopItems =  React.createClass({
    getInitialState : function(){
        return TopItem;
    },
    //コンテンツがrenderされた後1度だけ呼び出される
    componentDidMount : function(){
        $( APP ).on('toggleItemCol',function(e){
            var status = this.state.col === 1 ? 2 : 1;
            //全体を一旦透過その後再度表示
            $(document.getElementById('tile-child')).animate({opacity:0},function(){
                $(this).delay(100).animate({opacity:1});
            });
            //stateを更新
            setTimeout(function(){
                this.setState({
                    col  : status
                });
            }.bind(this),400);
            //再度並び替え
            setTimeout(function(){
                $(document.getElementById('tile-child')).wookmark(options);
            },500);
        }.bind(this));

        var options = {
            autoResize: true,
            flexibleWidth: 0,
            container: $( document.getElementById('tile-content') ),
            offset: 0,
            onLayoutChanged: false
        };
        setTimeout(function(){
            $(document.getElementById('tile-child')).wookmark(options);
        },1000);
        $(window).resize(function(){
            $(document.getElementById('tile-child')).wookmark(options);
        });
    },
    componentDidUpdate : function(){
        var options = {
            autoResize: true,
            flexibleWidth: 0,
            container: $( document.getElementById('tile-content') ),
            offset: 0,
            onLayoutChanged: false
        };
        setTimeout(function(){
            $(document.getElementById('tile-child')).wookmark(options);
        },10);
    },
    //Itemをクリックすることで詳細を表示
    clickItem : function(){
        $(APP).trigger('changeRoute',['Details']).trigger('DetailDisplayTop',['Details']).trigger('saveScrollPos');
    },
    clickFavolite : function( index ){
        this.state.items[index].favolite = this.state.items[index].favolite ? false : true;
        this.setState({
            items : this.state.items
        });
    },
    render : function(){
        if( this.props.asyncItems ) {
            this.state.items = this.props.asyncItems.items;
        }
        return(
            <ul id="tile-child" className="tiles">
                {this.state.items.map(function(value,index){
                    var favoliteClass = value.favolite ? 'favolite-btn on' : 'favolite-btn ';

                    if( !this.props.App.login ) {
                        favoliteClass = "";
                    }

                    var colClass = this.state.col === 1 ? 'col1' : '';
                    return (
                        <li key={index} className={colClass}>
                            <a onClick={this.clickItem}><img src={value.img} /></a>
                            <div onClick={this.clickFavolite.bind(this,index)} className={favoliteClass} ></div>
                        </li>
                    )
                }.bind(this))}
            </ul>
        )
    }
});

//トップページコンテンツ
var ContentTop = React.createClass({
    getInitialState : function() {
        return {
            data      : [],
            myChannel : true,
        };
    },
    clickMyChannel : function(e){
        e.preventDefault();
        this.state.myChannel = this.state.myChannel === false ? true : false;
        this.setState({
            myChannel : this.state.myChannel
        })
    },
    clickMore : function(e) {
        e.preventDefault();
        $.ajax({
            url: './json/topItem.json',
            dataType: 'json',
            cache: false,
            success: function( data ) {
                this.setState({
                    asyncItems : data
                });
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(status, err);
            }.bind(this)
        });
    },
    render:function(){
        var registerBtnDom = !this.props.App.login ? <div className="login-before page-transition-btn"><p><a href="#">会員登録はこちら</a></p></div> : '';

        if( this.state.myChannel ) {
            var myChannelDom = this.props.App.login ? <div className="rec-ch rec-ch-more-my"><div className="rec-ch-more add" onClick={this.clickMyChannel}><a href="#">Myチャンネルに追加</a></div></div> : '';
        } else {
            var myChannelDom = this.props.App.login ? <div className="rec-ch rec-ch-more-my"><div className="rec-ch-more delete" onClick={this.clickMyChannel}><a href="#">Myチャンネルから削除</a></div></div> : '';
        }
        return (
            <div id="top">
                <ContentTopMainVisual />
                <ContentTopChannel />
                <ContentChannelTab />
                {myChannelDom}
                <div className="rec-ch-desc">
                    <div id="tile-content" className="tilescontent">
                        <ReactTransitionGroup>
                        <ContentTopItems App={this.props.App} asyncItems={this.state.asyncItems} />
                        </ReactTransitionGroup>
                    </div>
                </div>
                <div className="rec-ch-more-btn" onClick={this.clickMore}><a href="#">もっと見る</a></div>
                <ContentChannelTab />
                {registerBtnDom}
            </div>
        )
    }
});

//商品詳細メインビジュアル
var DetailMainVisual = React.createClass({
    getInitialState : function(){
        return DetailMainVisualConf;
    },
    photoOpen : function(index,e){
        e.preventDefault();
        this.state.photoSwipeOptions.index = index;
        new PhotoSwipe(
            document.querySelectorAll('.pswp')[0],
            PhotoSwipeUI_Default,
            this.state.images,
            this.state.photoSwipeOptions
        ).init();
    },
    componentDidMount : function(){
        //初期化時に利用しないため処理を遅延
        // setTimeout(function(){
            $( this.getDOMNode() ).bxSlider({
                adaptiveHeight: false,
                mode: 'horizontal',
                infiniteLoop: false,
                hideControlOnEnd: true,
                pager: false,
                oneToOneTouch: true,
                useCSS: false
            });
        // }.bind(this),0);
    },
    render : function(){
        return (
            <ul className="bx-slider">
                {this.state.images.map(function(value,index){
                    return <li key={index} onClick={this.photoOpen.bind(this,index)}><a href="#"><img src={value.src} alt="" /></a></li>;
                }.bind(this))}
            </ul>
        )
    }
});

//商品詳細価格
var DetailProductCost = React.createClass({
    render : function(){
        return (
            <div className="product-list">
                <ul>
                    {this.props.data.map(function(value,index){
                        return <li key={index}><a href="#">{value.title}{'¥' + value.cost + ' + tax'}</a></li>
                    })}
                </ul>
            </div>
        )
    }
});

//商品詳細カタログ
var DetailCatalog = React.createClass({
    render : function(){
        return (
            <div className="catalog-link">
                <p>掲載カタログ{this.props.data.title}</p>
            </div>
        )
    }
});

//商品詳細類似商品
var DetailSimilarity = React.createClass({
    //コンテンツがrenderされた後1度だけ呼び出される
    componentDidMount : function(){
        var options = {
            autoResize: true,
            flexibleWidth: 0,
            container: $( document.getElementById('similar-tile-content') ),
            offset: 0,
            onLayoutChanged: false
        };
        setTimeout(function(){
            $(document.getElementById('similar-tile-child')).wookmark(options);
        },1000);
        $(window).resize(function(){
            $(document.getElementById('similar-tile-child')).wookmark(options);
        });
    },
    render : function(){
        return (
            <div className="similar-list">
                <h2>よく似ている商品</h2>
                <div id="similar-tile-content" className="tilescontent">
                    <ul id="similar-tile-child" className="tiles">
                        {this.props.data.map(function(value,index){
                            return <li><a href="#"><img src={value.image} alt="" /></a></li>
                        })}
                    </ul>
                </div>
                <div className="similar-list-more"><a href="#">もっと見る</a></div>
            </div>
        )
    }
});

//商品詳細関連ワード
var DetailWord = React.createClass({
    render : function(){
        return (
            <div className="relation-word">
                <h2>関連ワード</h2>
                <ul>
                    {this.props.data.map(function(value,index){
                        return <li><a href="#">{value.text}</a></li>
                    })}
                </ul>
                <div className="relation-word-more"><a href="#">関連ワード一覧</a></div>
            </div>
        )
    }
});

//商品詳細メイン
var ContentDetail = React.createClass({
    getInitialState : function(){
        return DetailInfo;
    },
    clickFavolite : function(){
        var status = this.state.favolite ? false : true;
        this.setState({
            favolite : status
        });
    },
    render : function(){
        var favoliteClass = this.state.favolite ? 'favolite-btn on' : 'favolite-btn ';
        return (
            <div id="detail">
                <div className="product-kv">
                    <ReactTransitionGroup component="div">
                        <DetailMainVisual data={this.state.MainVisual} key="detail-mainVisual"/>
                    </ReactTransitionGroup>
                    <div className={favoliteClass} onClick={this.clickFavolite.bind(this)}></div>
                </div>


                <div className="product-desc">
                    <a href="#">商品の詳細を見る</a>
                </div>

                <DetailProductCost  data={this.state.ProductCost} />
                <DetailCatalog      data={this.state.Catalog} />
                <DetailSimilarity   data={this.state.Similarity} />
                <DetailWord         data={this.state.Word} />

                <div className="add-favolite"><a href="#">お気に入りに追加</a></div>
                <div className="share">
                    <ul>
                        <li><a href="#"><img src="./img/icon_tw.png" /></a></li>
                        <li><a href="#"><img src="./img/icon_fb.png" /></a></li>
                        <li><a href="#"><img src="./img/icon_g.png" /></a></li>
                        <li><a href="#"><img src="./img/icon_line.png" /></a></li>
                    </ul>
                </div>
            </div>
        )
    }
});

//コンテンツメイン部分
var ContentMain = React.createClass({
    getPar : function(args){
        return args === 'Top' ? '0%' : '-100%';
    },
    componentDidMount : function(){
        $( this.getDOMNode() ).css('left',this.getPar('Top'));
        
        $( APP ).on('DetailDisplayTop',function(e,args){
            var type = args;

            $( this.getDOMNode() ).animate({'left': this.getPar(args) },300,function(){
                //migration 必要
                if( type !== 'Top'){
                    $(window).scrollTop(0);
                    $(this).css('top',0);
                    $(document.getElementById('top')).addClass('dis-none');
                }
            });
            //migration 必要
            if( type !== 'Top' ){
                $( this.getDOMNode() ).css('top',$(window).scrollTop());
            }else{
                $(document.getElementById('top')).removeClass('dis-none'); 
            }
        }.bind(this));
    },
    render : function(){
        return (
            <div className="l-main">
                <ReactTransitionGroup component="div">
                    <ModalContent key="content-modal"/>
                </ReactTransitionGroup>
                <ReactTransitionGroup component="div">
                    <ModalMenuContent key="content-modal-menu"/>
                </ReactTransitionGroup>
                <ContentTop App={this.props.App}/>
                <ContentDetail />
            </div>
        )
    }
});


//メインコンテンツ
var Content = React.createClass({
    componentDidUpdate: function(callback) {
        var par = this.props.Menu.status ? '-60%' : 0;
        $( this.getDOMNode() ).animate({ 'left' : par },250);
    },
    render:function(){
        return (
            <div id="content">
                <ContentHeader  Route={this.props.Route} App={this.props.App} Menu={this.props.Menu}/>
                <ReactTransitionGroup component="div">
                    <ContentMain Menu={this.props.Menu} Route={this.props.Route} App={this.props.App}/>
                </ReactTransitionGroup>
                <Footer />
            </div>
        )
    }
});

//フッターコンテンツ
var Footer = React.createClass({
    render:function(){
        return (
            <div className="l-footer">
                <ul>
                    <li><a href="#">ヘルプ</a></li>
                    <li><a href="#">利用規約</a></li>
                    <li><a href="#">プライバシーポリシー</a></li>
                </ul>
                <small>Copyright &copy; TOPPAN PRINTING CO., LTD.</small>
            </div>
        )
    }
});


var AppPrch = React.createClass({
    getInitialState : function(){
        //migration 必要
        var route = 'Top';
        if( location.hash === '#product' ){
            route = 'Details';
        }
        var login = location.hash === '#login' ? true : false;
        return {
            Route : route,
            Menu  : {
                status : false
            },
            scrollPos : 0,
            App : {
                login : login
            },
        }
    },
    componentDidMount : function(){
        $( APP ).on('changeRoute',function(e,args){
            this.setState({
                Route : args
            });

        }.bind(this));
        $( APP ).on('toggleMenu',function(e){
            var status = this.state.Menu.status ? false : true;
            this.setState({
                Menu  : {
                    status : status
                }
            });

            var c = {
                'fixClass'  : 'onFix',
                'flag'      : 'js-open',
                'beforePos' : 0, 
                'domain'    : '.menubar i',
                'fixContent': '.l-main'
            }
            if(this.state.Route === 'Top') {
                if(this.state.Menu.status === false) {
                    c.beforePos = $(window).scrollTop();
                    this.setState({
                        beforePos: c.beforePos
                    });

                    setTimeout(function(){
                        $(c.fixContent).addClass(c.fixClass).css('top',-c.beforePos);
                    },200);
                } else {
                    $(c.fixContent).removeClass(c.fixClass).css('top','');
                    $(window).scrollTop(this.state.beforePos);
                }
            } else {
                if(this.state.Menu.status === false) {
                    c.beforePos = $(window).scrollTop();
                    this.setState({
                        beforePos: c.beforePos
                    });

                    setTimeout(function(){
                        $(c.fixContent).addClass(c.fixClass + 'Detail').css('top',-c.beforePos);
                    },200);
                } else {
                    $(c.fixContent).removeClass(c.fixClass + 'Detail').css('top','');
                    $(window).scrollTop(this.state.beforePos);
                }
            }

        }.bind(this));
        //migration必要かも
        $( APP ).on('saveScrollPos',function(e){
            this.setState({
                scrollPos  : $(window).scrollTop()
            });
        }.bind(this));
        //migration必要かも
        $( APP ).on('fireScrollPos',function(e){
            $(window).scrollTop( this.state.scrollPos );
        }.bind(this));
    },
    render: function(){
        var menuKey = this.state.Menu.status ? 'menu_active' : 'menu_passive';
        return (
            <div className="contentWrap">
                <ReactTransitionGroup component="div">
                    <Menu Menu={this.state.Menu} key='menu' />
                </ReactTransitionGroup>
                <ReactTransitionGroup component="div">
                    <Content Menu={this.state.Menu} Route={this.state.Route} App={this.state.App}key='content' />
                </ReactTransitionGroup>
            </div>
        )
    }
});

/*---------------------------
  @render
----------------------------*/
React.render(
    <AppPrch />,
    document.getElementById('wrapper')
);

;(function($,window,c){
    $(function(){
        $(document).on('click',c.domain,function(){
            var mvContentOfs = $(c.mvContent).offset();
            $('html,body').animate({scrollTop: mvContentOfs.top - 50});
        });
    });
})(jQuery,window,{
    'domain'   : '.rec-ch li a',
    'mvContent': '.rec-ch-tabmenu'
});

;(function($,window,c){
    $(function() {
        var targetoft = "";
        $(document).on('scroll', function() {
            targetoft = $('.login-before.page-transition-btn').get(0).offsetTop;
            if ( $(this).scrollTop() + 899 < targetoft ) {
                $('.login-before.page-transition-btn a').css('position', 'fixed');
            } else {
                $('.login-before.page-transition-btn a').css('position', 'relative');
            }
        });
    });
})(jQuery,window,{
});

;(function($,window,c){
    $(function() {
        var count = 0;
        var test  = "";
        var last  = "";
        $('.tab-wrap').find('span').on('click',function(e) {
            var onClass = $(this).attr("class");

            if(last == "リラックス" && $(this).text() == "モード") {
                $('.tab-relax').removeClass('on');
                $('.tab-happy').addClass('on');
            }

            if($(this).text() == "カジュアル" && $(this).prev().hasClass("on")) {
                $('.' + onClass).eq(1).prev().removeClass('on');
                $('.' + onClass).eq(1).next().removeClass('on');
                $('.' + onClass).eq(1).addClass('on');

                $('.' + onClass).eq(0).prev().removeClass('on');
                $('.' + onClass).eq(0).next().removeClass('on');
                $('.' + onClass).eq(0).addClass('on');
                return;
            }
            if($(this).text() == "ハッピー" && $(this).next().hasClass("on")) {
                $('.' + onClass).eq(1).prev().removeClass('on');
                $('.' + onClass).eq(1).next().removeClass('on');
                $('.' + onClass).eq(1).addClass('on');

                $('.' + onClass).eq(0).prev().removeClass('on');
                $('.' + onClass).eq(0).next().removeClass('on');
                $('.' + onClass).eq(0).addClass('on');
                return;
            }

            if( $(this).hasClass('on') || $(this).text() == "キュート" || $(this).text() == "リラックス")  {
                e.preventDefault();
            } else {
                if($(this).next().hasClass('on')) {
                    count--;
                    $('.tab-wrap').find('span').animate({left: + 11.1111 * -count + '%'});
                } else {
                    count++;
                    $('.tab-wrap').find('span').animate({left: - 11.1111 * count + '%'});
                }
            }

            $('.' + onClass).eq(1).prev().removeClass('on');
            $('.' + onClass).eq(1).next().removeClass('on');
            $('.' + onClass).eq(1).addClass('on');

            $('.' + onClass).eq(0).prev().removeClass('on');
            $('.' + onClass).eq(0).next().removeClass('on');
            $('.' + onClass).eq(0).addClass('on');
            last = $(this).text();
        });

        $('#rec-child').find('li').on('click', function() {
            $('.tab-wrap').find('span').css("left", "0%");
            if( $(this).find('img').attr("alt") == "キュート" ) {
                $('.tab-cute').eq(1).prev().removeClass('on');
                $('.tab-cute').eq(1).next().removeClass('on');
                $('.tab-cute').eq(1).addClass('on');

                $('.tab-cute').eq(0).prev().removeClass('on');
                $('.tab-cute').eq(0).next().removeClass('on');
                $('.tab-cute').eq(0).addClass('on');
                count = 0;
            } else if( $(this).find('img').attr("alt") == "カジュアル" ) {
                $('.tab-casual').eq(1).prev().removeClass('on');
                $('.tab-casual').eq(1).next().removeClass('on');
                $('.tab-casual').eq(1).addClass('on');

                $('.tab-casual').eq(0).prev().removeClass('on');
                $('.tab-casual').eq(0).next().removeClass('on');
                $('.tab-casual').eq(0).addClass('on');
                count = 1;
            } else if( $(this).find('img').attr("alt") == "ビジネス" ) {
                $('.tab-business').eq(1).prev().removeClass('on');
                $('.tab-business').eq(1).next().removeClass('on');
                $('.tab-business').eq(1).addClass('on');

                $('.tab-business').eq(0).prev().removeClass('on');
                $('.tab-business').eq(0).next().removeClass('on');
                $('.tab-business').eq(0).addClass('on');
                $('.tab-wrap').find('span').animate({left: - 11.1111 * 1 + '%'});
                count = 1;
            } else if( $(this).find('img').attr("alt") == "インテリア" ) {
                $('.tab-interior').eq(1).prev().removeClass('on');
                $('.tab-interior').eq(1).next().removeClass('on');
                $('.tab-interior').eq(1).addClass('on');

                $('.tab-interior').eq(0).prev().removeClass('on');
                $('.tab-interior').eq(0).next().removeClass('on');
                $('.tab-interior').eq(0).addClass('on');
                $('.tab-wrap').find('span').animate({left: - 11.1111 * 2 + '%'});
                count = 2;
            } else if( $(this).find('img').attr("alt") == "ブラック" ) {
                $('.tab-black').eq(1).prev().removeClass('on');
                $('.tab-black').eq(1).next().removeClass('on');
                $('.tab-black').eq(1).addClass('on');

                $('.tab-black').eq(0).prev().removeClass('on');
                $('.tab-black').eq(0).next().removeClass('on');
                $('.tab-black').eq(0).addClass('on');
                $('.tab-wrap').find('span').animate({left: - 11.1111 * 3 + '%'});
                count = 3;
            } else if( $(this).find('img').attr("alt") == "モード" ) {
                $('.tab-mode').eq(1).prev().removeClass('on');
                $('.tab-mode').eq(1).next().removeClass('on');
                $('.tab-mode').eq(1).addClass('on');

                $('.tab-mode').eq(0).prev().removeClass('on');
                $('.tab-mode').eq(0).next().removeClass('on');
                $('.tab-mode').eq(0).addClass('on');
                $('.tab-wrap').find('span').animate({left: - 11.1111 * 4 + '%'});
                count = 4;
            } else if( $(this).find('img').attr("alt") == "ハッピー" ) {
                $('.tab-happy').eq(1).prev().removeClass('on');
                $('.tab-happy').eq(1).next().removeClass('on');
                $('.tab-happy').eq(1).addClass('on');

                $('.tab-happy').eq(0).prev().removeClass('on');
                $('.tab-happy').eq(0).next().removeClass('on');
                $('.tab-happy').eq(0).addClass('on');
                $('.tab-wrap').find('span').animate({left: - 11.1111 * 5 + '%'});
                count = 5;
            } else if( $(this).find('img').attr("alt") == "リラックス" ) {
                $('.tab-relax').eq(1).prev().removeClass('on');
                $('.tab-relax').eq(1).next().removeClass('on');
                $('.tab-relax').eq(1).addClass('on');

                $('.tab-relax').eq(0).prev().removeClass('on');
                $('.tab-relax').eq(0).next().removeClass('on');
                $('.tab-relax').eq(0).addClass('on');
                $('.tab-wrap').find('span').animate({left: - 11.1111 * 5 + '%'});
                count = 5;
            }
        });
    });
})(jQuery,window,{

});